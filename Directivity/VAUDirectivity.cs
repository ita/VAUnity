﻿using System;
using UnityEngine;
using VA;

namespace VAUnity
{
    public class VAUDirectivity : VAUObject<int>
    {
        [Tooltip("Absolute or relative path (relative to Assets folder), also use AddSearchPath for special folders.")]
        public string FilePath = "";

        [Tooltip("Versatile name for this directivity")]
        public string Name = "";

        protected override void Awake()
        {
            base.Awake();

            if (!_va.IsConnected())
            {
                Debug.LogError("Could not enable directivity, not connected to VA");
                return;
            }

            if (FilePath.Length > 0)
                _id = _va.CreateDirectivityFromFile(FilePath);
        }

        private void OnDestroy()
        {
            if (!_va.IsConnected())
                return;

            if (_id != 0)
                _va.DeleteDirectivity(_id);
        }
    }
}
