﻿using UnityEngine;

namespace VAUnity
{
    public class VAUSoundReceiver : VAUSoundObject {
        [Tooltip("Set an explicit renderer for this receiver")]
        [SerializeField] protected VAUAudioRenderer audioRenderer;

        [Tooltip("Connect an directivity to this receiver")]
        public VAUDirectivity directivity;

        public VAUAudioRenderer ExplicitRenderer => audioRenderer;

        private void Start()
        {
            // Do not create SoundReceiver in Awake(), it might depend on an uninitialized Renderer!
            _id = audioRenderer ? _va.CreateSoundReceiverExplicitRenderer(audioRenderer.ID, Name)
                                : _va.CreateSoundReceiver(Name);
            // TODO: Use Auralization Mode String stuff to select AuraMode in Editor
            // _va.SetSoundReceiverAuralizationMode(_id, "all");
            
            if (directivity)
            {
                if(directivity.ID < 1)
                    Debug.LogError("Receiver Directivity not init yet.");
                    
                _va.SetSoundReceiverDirectivity(_id, directivity.ID);
            }
            else
            {
                Debug.LogWarning("No Directivity found, please assign Directivity, otherwise Spacialzation will not work.");
            }
            
            UpdateSoundReceiverPositionOrientation();
        }


        void Update()
        {
            if (transform.hasChanged)
                UpdateSoundReceiverPositionOrientation();
        }

        // Uses the View- and Up-Vector to transmit the position of the listener to VA


        protected void UpdateSoundReceiverPositionOrientation()
        {
            SetSoundReceiverPositionOrientation(out var vaPosition, out var vaOrientationView,
                out var vaOrientationUp);
            _va.SetSoundReceiverPosition(_id, vaPosition);
            _va.SetSoundReceiverOrientationVU(_id, vaOrientationView, vaOrientationUp);
        }

        private void OnDestroy()
        {
            _va.DeleteSoundReceiver(ID);
        }
    }
}
