﻿using System;
using UnityEngine;
using VA;

namespace VAUnity
{
    [RequireComponent(typeof(VAUSoundReceiver))]
    public class VAUHeadDimensions : MonoBehaviour
    {
        [Tooltip("Anthropometric head width, for HRIR individualization or generic binaural cues")] [Range(0, 0.3f)]
        public double headWidth = 0.12;

        [Tooltip("Anthropometric head height, for HRIR individualization or generic binaural cues")] [Range(0, 0.3f)]
        public double headHeight = 0.10;

        [Tooltip("Anthropometric head depth, for HRIR individualization or generic binaural cues")] [Range(0, 0.3f)]
        public double headDepth = 0.15;

        private double _shadowHeadWidth = -1;
        private double _shadowHeadHeight = -1;
        private double _shadowHeadDepth = -1;

        private VANet _va;
        private VAUSoundReceiver _receiver;

        private void Awake()
        {
            _va = VAUnity.VA;
            _receiver = GetComponent<VAUSoundReceiver>();
        }


        void Update()
        {
            if (Math.Abs(headWidth - _shadowHeadWidth) > 1e-5 || Math.Abs(headHeight - _shadowHeadHeight) > 1e-5 ||
                Math.Abs(headDepth - _shadowHeadDepth) > 1e-5)
            {
                if (_receiver.ID == 0)
                    Debug.LogError("Receiver not init yet.", this);
                _va.SetSoundReceiverAnthropometricData(_receiver.ID, headWidth, headHeight, headDepth);
                _shadowHeadWidth = headWidth;
                _shadowHeadHeight = headHeight;
                _shadowHeadDepth = headDepth;
            }
        }
    }
}
