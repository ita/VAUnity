﻿using UnityEngine;
using VA;

namespace VAUnity
{
    public abstract class VAUSoundObject : VAUObject<int>
    {
        [Tooltip("Descriptive name")]
        public string Name = "SoundReceiver";

        protected void SetSoundReceiverPositionOrientation(out VAVec3 vaPosition,out VAVec3 vaOrientationView,
            out VAVec3 vaOrientationUp)
        {
            var t = transform;
            var q = t.rotation;
            var p = t.position;
            var up = q * Vector3.up;
            var view = q * Vector3.forward;
            vaPosition = new VAVec3(p);
            vaOrientationView = new VAVec3(view);
            vaOrientationUp = new VAVec3(up);
        }

    }
}
