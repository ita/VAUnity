﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace VAUnity
{
	/// <summary>
	/// Abstract Sound Source Interface with basic functionality for sound sources, e.g. updating position and
	/// orientation, handling Directivity, handling volume and mute updates.  
	/// </summary>
	public abstract class VAUSoundSource : VAUSoundObject
	{
		[Tooltip("Set an explicit renderer for this source")]
		[SerializeField] protected VAUAudioRenderer audioRenderer;
		
		[Tooltip("custom VAUDirectivity Script.")] 
		[SerializeField] protected VAUDirectivity directivity = null;

		[Tooltip("Invoke Event if sound state is changing from not playing to playing. Not effected by mute.")]
		[SerializeField] public UnityEvent onSoundStart;

		[Tooltip("Invoke Event if sound state is changing from playing to not playing. Not effected by mute.")]
		[SerializeField] public UnityEvent onSoundStops;

		// todo Set an explicit renderer for this source
		// [Tooltip("Set an explicit renderer for this source")]
		// public VAUAudioRenderer Renderer = null;

		public void SetDirectivity(VAUDirectivity directivity)
		{
			this.directivity = directivity;
		}
		
		public abstract bool IsLooping { get; }
		public abstract string SignalSourceId { get; }

		protected bool _isPlaying = false;

		private float _volumeShadow;
		private bool _muteShadow;

		/// <summary>
		/// Play Sound Source
		/// </summary>
		public virtual void Play()
		{
			onSoundStart?.Invoke();
			_isPlaying = true;
		}
		
		/// <summary>
		/// Pause Sound Source
		/// </summary>
		public virtual void Pause()
		{
			_isPlaying = false;
			onSoundStops?.Invoke();
		}

		/// <summary>
		/// Stop Sound Source
		/// </summary>
		public virtual void Stop()
		{
			_isPlaying = false;
			onSoundStops?.Invoke();
		}
		
		/// <summary>
		/// if signal source is playing back or not
		/// </summary>
		/// <returns>true if sound source in VA is running and false if not</returns>
		public abstract bool GetPlaybackState();

		/// <summary>
		/// Helper to see if signal source is playing back or not
		/// </summary>
		/// <param name="signalSourceId">input SignalSource</param>
		/// <returns>true if sound source in VA is running and false if not</returns>
		protected bool GetPlaybackState(string signalSourceId)
		{
			var state= _va.GetSignalSourceBufferPlaybackState(signalSourceId);
			switch (state)
			{
				case "PLAYING":
					return true;
				case "STOPPED":
				case "PAUSED":
					return false;
				default:
					Debug.LogError("Invalid Signal source State: "+state,this);
					return false;
			}
		}

		/// <summary>
		/// Returns Signal Source ID
		/// </summary>
		/// <returns>signal source Id</returns>
		public string GetSignalSourceFromSoundSource()
		{
			return _va.GetSoundSourceSignalSource(_id);
		}

		/// <summary>
		/// Set Mute Status of Sound Source
		/// </summary>
		/// <param name="mute"></param>
		public void SetMuted(bool mute)
		{
			_va.SetSoundSourceMuted(_id, mute);
			_muteShadow = mute;
		}

		/// <summary>
		/// create SoundSource
		/// </summary>
		protected override void Awake()
		{
			base.Awake();
			
			// _id = _va.CreateSoundSource(this.name);
		}

		/// <summary>
		/// Set Initial Position and Orientation + Add Directivity
		/// </summary>
		protected virtual void Start()
		{
			// Do not create SoundSource in Awake(), it might depend on an uninitialized Renderer! 
			if (audioRenderer == null || !audioRenderer.IsSoundSourceExplicitRenderer)
				_id =_va.CreateSoundSource(Name);
			else
				_id = audioRenderer.CreateSoundSourceExplicitRenderer();
		
			if (directivity)
				_va.SetSoundSourceDirectivity(_id, directivity.ID);
			else if (GetComponent<VAUDirectivity>())
				_va.SetSoundSourceDirectivity(_id, GetComponent<VAUDirectivity>().ID);
			
			// Initially, set the pose (otherwise rendering module can not spatialize)
			SetSoundSourcePositionOrientation();
		}

		/// <summary>
		/// Update Position and Orientation if changed
		/// </summary>
		protected virtual void Update()
		{
			if (transform.hasChanged)
				SetSoundSourcePositionOrientation();
			transform.hasChanged = false;

			if (_isPlaying && !IsLooping)
			{
				if (GetPlaybackState()) return;
				
				_isPlaying = false;
				onSoundStops?.Invoke();

			}
		}

		/// <summary>
		/// Stop and delete Signal source and sound source at the end
		/// </summary>
		protected virtual void OnDestroy()
		{
			//_VA = VAUnity.VA;
			if (_va.IsConnected())
			{
				Stop();
				_va.SetSoundSourceSignalSource(_id, "");
				_va.DeleteSoundSource(ID);
			}
		}
		
		/// <summary>
		/// Updates Sound Source Position Orientation
		/// </summary>
		private void SetSoundSourcePositionOrientation()
		{
			SetSoundReceiverPositionOrientation(out var vaPosition, out var vaOrientationView,
				out var vaOrientationUp);
			_va.SetSoundSourcePosition(_id, vaPosition);
			_va.SetSoundSourceOrientationVU(_id, vaOrientationView, vaOrientationUp);
		}

		/// <summary>
		/// Updates Sound Volume and mute unmute on change
		/// </summary>
		/// <param name="volumeWatts">volume in watts</param>
		/// <param name="mute">is muted</param>
		/// <param name="forceUpdate">update without comparing to last values.</param>
		protected void UpdateSoundSource(float volumeWatts, bool mute, bool forceUpdate = false)
		{
			if (Math.Abs(_volumeShadow - volumeWatts) > 1e-5 || forceUpdate)
			{
				_va.SetSoundSourceSoundPower(_id, volumeWatts); 
				_volumeShadow = volumeWatts;
			}

			if (_muteShadow != mute || forceUpdate)
			{
				_va.SetSoundSourceMuted(_id, mute);
				_muteShadow = mute;
			}
		}
	}
}
