﻿using UnityEngine;
using VA;

namespace VAUnity
{
    public class VAUTextToSpeechSignalSource : VAUSignalSource
    {
        [Tooltip("Text which should be read out.")]
        public string text = "hi there, my name is rachel. virtual acoustics is a real-time auralization framework " +
                             "for scientific research in Virtual Reality created by the institute of technical " +
                             "acoustics, RWTH Aachen university. thank you for testing the VA unity C sharp scripts " +
                             "for controlling a VA server.";
        public bool directPlayback = false;

        private string _textIdentifier = "";

        public override bool IsLooping => false;

        protected override void Awake()
        {
            base.Awake();

            if (!_va.IsConnected())
            {
                Debug.LogError( "Could not create text to speech signal source, not connected to VA" );
                return;
            }
            
            _id = _va.CreateSignalSourceTextToSpeech(name);
            
            _va.TextToSpeechPrepareText(_id, _textIdentifier, text);

            if (directPlayback)
                Play();
        }

        public override void Play()
        {
            _va.TextToSpeechPlaySpeech(_id, _textIdentifier);
        }

        public override void Pause()
        {
            base.Pause();
        }

        public override void Stop()
        {
            base.Stop();
        }

    }
}
