﻿using UnityEngine;
using System.IO;

namespace VAUnity
{
	public class VAUSignalSourceAudioFile : VAUSignalSource
	{
		[Tooltip("Absolute or relative file path (relative to project Root folder, Package folder or any folder added to search path using AddSearchPath)")]
		[SerializeField] private string filePath;
		
		[Tooltip("Will loop the audio signal source.")]
		public bool isLooping = true;

		public override bool IsLooping => isLooping;

		protected override void Awake()
		{
			base.Awake();
			
			//GetAbsolutePath();
			
			_id = _va.CreateSignalSourceBufferFromFile(filePath, name);
			Debug.Assert(_id.Length > 0, "Could not create audio file signal source '" + name + "' from file path " + filePath);
			
			//Debug.Log("Created audio file signal source with id '" + _ID + "'");
			_va.SetSignalSourceBufferLooping (_id, isLooping);
		}
		
		/// <summary>
		/// Get the direct path to the VA Server
		/// </summary>
		private void GetAbsolutePath()
		{
			filePath = Path.GetFullPath(filePath);
			Debug.Log("SignalSourceAudioFile absolute path:" + filePath);
		}
	}
}
