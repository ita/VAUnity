﻿using UnityEngine;

namespace VAUnity
{
    public abstract class VAUSignalSource : VAUObject<string>
    {
        public abstract bool IsLooping { get; }
        
        public virtual void Play()
        {
            _va.SetSignalSourceBufferPlaybackAction(_id, "play");
        }
        public virtual void Pause()
        {
            _va.SetSignalSourceBufferPlaybackAction(_id, "pause");
        }
        public virtual void Stop()
        {
            _va.SetSignalSourceBufferPlaybackAction(_id, "stop");
        }
        

        void OnDestroy()
        {
            if (_id.Length > 0)
                _va.DeleteSignalSource(_id);
        }

    }
}
