﻿
using UnityEngine;

namespace VAUnity
{
    /// <summary>
    /// Create a Sound Source and Signal Source in VA based on the Audio Source in Unity.
    /// injected properties are volume (gain converted to watts), mute status, audio clip, loop, playOnAwake
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class VAUSoundSourceAudioSource : VAUSoundSource
    {
        private string _signalSourceId = null;
        private AudioSource _audioSource;
        public override bool IsLooping => _audioSource.loop;
        public override string SignalSourceId => _signalSourceId;

        public string audioFileForTesting = "";


        protected override void Awake()
        {
            base.Awake();
            _audioSource = GetComponent<AudioSource>();
        }

        /// <summary>
        /// Init Sound Source and Signal Source in VA
        /// </summary>
        protected override void Start()
        {
            // Volume of 1.0 results in default sound power: 31.67 mW -> 94 dB SPL @ 1m
            //UpdateSoundSource(_audioSource.volume * 31.69e-3f, _audioSource.mute, true);


            base.Start();
            
            // Create and connect audio signal source
            if (_audioSource.clip)
            {
                //var filePath = AssetDatabase.GetAssetPath(_audioSource.clip);
                var sName = _audioSource.clip.name + "_signal";
                //CreateSignalSource(filePath, sName);
            }
            else
            {
                if(audioFileForTesting!="")
                    CreateSignalSource(audioFileForTesting, name);
                else
                    Debug.LogError("No Audioclip in AudioSource.", this);
            }
            
            transform.hasChanged = true;
        }

        private void CreateSignalSource(string filePath, string sName)
        {
            //var playOnAwake = _audioSource.playOnAwake;

            _signalSourceId = _va.CreateSignalSourceBufferFromFile(filePath, sName);
            Debug.Assert(_signalSourceId.Length > 0,
                "Could not create integrated audio file signal source '" + sName + "' file from path " + filePath);
            _va.SetSignalSourceBufferLooping(_signalSourceId, IsLooping);
            
            _va.SetSoundSourceSignalSource(_id, _signalSourceId);
            //if (playOnAwake)
            //    Play();
        }


        /// <summary>
        /// Get Called every frames and update changes in Sound Power and mute status
        /// </summary>
        protected override void Update()
        {
            base.Update();
            // Volume of 1.0 results in default sound power: 31.67 mW -> 94 dB SPL @ 1m
            //UpdateSoundSource(_audioSource.volume * 31.69e-3f, _audioSource.mute);
        }


        protected override void OnDestroy()
        {
            base.OnDestroy();
            
            if (_va.IsConnected())
            {
                _va.SetSoundSourceSignalSource(_id, "");
                _va.DeleteSoundSource(ID);

                // Temptative signal source deletion
                if (_signalSourceId != null)
                    _va.DeleteSignalSource(_signalSourceId);
            }
        }

        /// <summary>
        /// Play Sound Source
        /// </summary>
        public override void Play()
        {
            base.Play();
            _va.SetSignalSourceBufferPlaybackAction(_signalSourceId, "play");
        }  
        
        /// <summary>
        /// Pause Sound Source
        /// </summary>
        public override void Pause()
        {
            base.Pause();
            _va.SetSignalSourceBufferPlaybackAction(_signalSourceId, "pause");
        }

        /// <summary>
        /// Stop Sound Source
        /// </summary>
        public override void Stop()
        {
            base.Stop();
            _va.SetSignalSourceBufferPlaybackAction(_signalSourceId, "stop");
        }

        /// <summary>
        /// if signal source is playing back or not
        /// </summary>
        /// <returns>true if sound source in VA is running and false if not</returns>
        public override bool GetPlaybackState()
        {
            return GetPlaybackState(_signalSourceId);
        }
    }
}