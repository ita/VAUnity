﻿using System;
using UnityEngine;

namespace VAUnity
{
    /// <summary>
    /// Create a Sound Source in VA based on the inspector settings
    /// </summary>
    public class VAUSoundSourceGeneric : VAUSoundSource
    {
        [Tooltip("Insert a custom VAUSignalSource-Script.")]
        [SerializeField] private VAUSignalSource signalSource;
		
        [Tooltip("Sound power in Watts (default is 31mW)")]
        [Min(0)][SerializeField] private float soundPower = 0.0031f;
		
        [Tooltip("Mutes the sound source")]
        [SerializeField] private bool isMuted = false;

        [Tooltip("Will immediately start the signal source payback.")]
        [SerializeField] protected bool playOnStart = false;

        public override bool IsLooping => signalSource.IsLooping;

        public override string SignalSourceId => signalSource.ID;

        /// <summary>
        /// Init Sound Source and Signal Source in VA
        /// </summary>
        protected override void Start()
        {
            base.Start();
			
            _va.SetSoundSourceAuralizationMode(_id, "all");
            UpdateSoundSource(soundPower, isMuted, true);
            // Connect to directivity, if linked or attached
            // Connect to signal source, if linked or attached
            if (signalSource)
            {
                _va.SetSoundSourceSignalSource(_id, signalSource.ID);
            }
            else
            {
                Debug.LogError("Signal Source not found", this);
            }
            
            if(playOnStart)
                Play();
        }

        /// <summary>
        /// Get Called every frames and update changes in Sound Power and mute status
        /// </summary>
        protected override void Update()
        {
            base.Update();
            UpdateSoundSource(soundPower, isMuted);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            //_VA = VAUnity.VA;
            if (_va.IsConnected())
            {
                // Temptative signal source deletion
                if (signalSource.ID != null)
                    _va.DeleteSignalSource (signalSource.ID);
            }
        }

        /// <summary>
        /// Play Sound Source
        /// </summary>
        public override void Play()
        {
            base.Play();
            signalSource.Play();
        }
        
        /// <summary>
        /// Pause Sound Source
        /// </summary>
        public override void Pause()
        {
            base.Pause();
            signalSource.Pause();
        }
        
        /// <summary>
        /// Stop Sound Source
        /// </summary>
        public override void Stop()
        {
            base.Stop();
            signalSource.Stop();
        }
        
        /// <summary>
        /// if signal source is playing back or not
        /// </summary>
        /// <returns>true if sound source in VA is running and false if not</returns>
        public override bool GetPlaybackState()
        {
            return GetPlaybackState(signalSource.ID);
        }

        /// <summary>
        /// Updates the Signal Source of the SoundSource.
        /// Note: Playback properties of the SignalSource will *NOT* be changed! 
        /// </summary>
        /// <param name="newSignalSource">SignalSource that shall be connected to the SoundSource</param>
        public void SetSignalSource(VAUSignalSource newSignalSource)
        {
            if (!newSignalSource)
            {
                Debug.LogError("Setting signal source for SoundSource " + ID +" failed: SignalSource is not initialized!");
                return;
            }
            
            signalSource = newSignalSource;
            _va.SetSoundSourceSignalSource(ID, signalSource.ID);
        }

    }
}