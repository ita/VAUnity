﻿using System;
using System.Diagnostics;
using System.IO;
using UnityEngine;
using Application = UnityEngine.Application;
using Debug = UnityEngine.Debug;

namespace VAUnity
{
    /// <summary>
    /// Starts VA automatically when Unity starts, get called by VAUnity 
    /// </summary>
    [CreateAssetMenu(fileName = "VAStarter", menuName = "VAUnity/VAStarter", order = 1)]
    public class VAStarter : ScriptableObject
    {
        [Tooltip("Directory of VAServer. Can be absolute or relative to project root directory.")] 
        [SerializeField] private string vaWorkingDirectory = "Packages/com.ihta.vaunity/Resources/VAServer_v2022a/";
        //[SerializeField] private string vaWorkingDirectory ="./Assets/Plugins/vaunity_package/Resources/VAServer_v2022a/";

        [Tooltip("Config File relative to the location of this Object.")]
        [SerializeField] private string vaPathConfig = "VACore.ini";

        [SerializeField] private bool startForeground = true;
        
        /// <summary>
        /// relative path from working directory to config path
        /// </summary>
        [HideInInspector] public string _configPath = "conf";
        

        // [DllImport("user32.dll")]
        // static extern bool PostMessage(IntPtr hWnd, UInt32 Msg, int wParam, int lParam);
        private Process _process = null;
        private StreamWriter _messageStream;
        
        private bool _isInit = false;
        private bool _stopOnDestroy = false;

        private void OnDestroy()
        {
            if (_stopOnDestroy)
                StopServer();
        }

        /// <summary>
        /// Starts VA Server with chosen Config file
        /// </summary>
        public void Init()
        {
            if (_isInit) return;
            GetWorkingDirectoryPath();
            //GetConfigPath();
            if (startForeground)
                StartProcessForeground("bin\\VAServer.exe localhost:12340 " + _configPath +"\\"+ vaPathConfig);
            else
                StartProcessBackground("bin\\VAServer.exe localhost:12340 " + _configPath + "\\" + vaPathConfig +
                                       " allow_remote_control_shutdown");
            
            _isInit = true;
        }

        public void StartServerCustomConf(string absVaDir, string relConfDir)
        {
            vaWorkingDirectory = absVaDir;

            _configPath = Application.dataPath + "\\..\\" + relConfDir;
            if(!File.Exists(_configPath))
                Debug.LogError("Config File not found." + _configPath, this);
            _configPath = GetRelativePath(_configPath, vaWorkingDirectory);
            StartProcessBackground("bin\\VAServer.exe localhost:12340 " + _configPath);
            _stopOnDestroy = true;
        }
        
        /// <summary>
        /// Check if paths and files exists exists
        /// </summary>
        [ContextMenu("Check Paths")]
        public void CheckPaths()
        {
            if(!Directory.Exists(vaWorkingDirectory))
                Debug.LogWarning("Cannot find VA Dir " + vaWorkingDirectory);
            
            if(!Directory.Exists(_configPath))
                Debug.LogWarning("Cannot find Config Path " + _configPath);
        }

        public void StopServer()
        {
            _isInit = false;
            
            // _process.WaitForExit();
            if (_process != null && !_process.HasExited )
            {
                _process.Kill();
            }
            var processes = Process.GetProcessesByName("VAServer");
            foreach (var process in processes)
            {
                Debug.Log("Kill Process " + process.ProcessName);
                process.Kill();
            }
            
        }

        /// <summary>
        /// Get the direct path to the VA Server
        /// </summary>
        private void GetWorkingDirectoryPath()
        {
            // check for integration via package or plugin
            if (!Directory.Exists(Path.GetFullPath(vaWorkingDirectory)))
            {
                Debug.Log("Working directory not found. Trying integration via Packages folder. Invalid directory: " + vaWorkingDirectory);
                vaWorkingDirectory = "Packages/com.ihta.vaunity/Resources/VAServer_v2022a/";
                if (!Directory.Exists(Path.GetFullPath(vaWorkingDirectory)))
                {
                    Debug.Log("Working directory not found. Trying integration via plugins folder. Invalid directory: " + vaWorkingDirectory);
                    vaWorkingDirectory = "./Assets/Plugins/vaunity_package/Resources/VAServer_v2022a/";
                    if (!Directory.Exists(Path.GetFullPath(vaWorkingDirectory)))
                        Debug.Log("Invalid directory: " + vaWorkingDirectory + " Check VA Integration.");   
                }
            }
            
            vaWorkingDirectory = Path.GetFullPath(vaWorkingDirectory);
            //Debug.Log("Working directory absolute path:" + vaWorkingDirectory);
        }
        
        /*
        /// <summary>
        /// Calculate the relative path of the config file
        /// </summary>
        private void GetConfigPath()
        {
            //_configPath = Path.GetDirectoryName(Application.dataPath + "/../" + AssetDatabase.GetAssetPath(this));
            
            //var exists = File.Exists(_configPath +"/"+ vaPathConfig);
            _configPath = GetRelativePath(_configPath, vaWorkingDirectory);
            //Debug.Log("bin\\VAServer.exe localhost:12340 " + _configPath +"\\"+ vaPathConfig);
            //return exists;
        }*/

        /// <summary>
        /// Returns a relative path from one path to another.
        /// </summary>
        /// <param name="folder">The destination path.</param>
        /// <param name="relativeTo">The source path the result should be relative to. This path is always considered to be a directory.</param>
        /// <returns></returns>
        private string GetRelativePath(string folder, string relativeTo)
        {
            var pathUri = new Uri(folder);
            // Folders must end in a slash
            if (!relativeTo.EndsWith(Path.DirectorySeparatorChar.ToString()))
            {
                relativeTo += Path.DirectorySeparatorChar;
            }
            var folderUri = new Uri(relativeTo);
            return Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString().Replace('/', Path.DirectorySeparatorChar));
        }

        /// <summary>
        /// Start VA Server as a Windows Process
        /// </summary>
        private void StartProcessForeground(string command)
        {
            try
            {
                if(!Directory.Exists(vaWorkingDirectory))
                    Debug.LogError("Cannot find VA Directory: " + vaWorkingDirectory);
                if(!File.Exists(vaWorkingDirectory + "bin\\VAServer.exe"))
                    Debug.LogError("Cannot find bin\\VAServer.exe in Directory: " + vaWorkingDirectory);
                    
                _process = new Process
                {
                    EnableRaisingEvents = false,
                    StartInfo =
                    {
                        WorkingDirectory = vaWorkingDirectory,
                        FileName = "cmd.exe",
                        // Arguments = "bin\\VAServer.exe localhost:12340 " + vaPathConfig,
                        Arguments = "/c" + command,
                        UseShellExecute = true,
                        // CreateNoWindow = true,
                        RedirectStandardOutput = false,
                        // RedirectStandardInput = true,
                        RedirectStandardError = false
                    }
                };
                _process.Start();
                Debug.Log("Successfully launched app in foreground");
            }
            catch (Exception e)
            {
                Debug.LogError("Unable to launch app: " + e.Message);
            }
        }

        /// <summary>
        /// Start VA Server as a Windows Process
        /// </summary>
        private void StartProcessBackground(string command)
        {
            try
            {
                if (!Directory.Exists(vaWorkingDirectory))
                    Debug.LogError("Cannot find VA Dir " + vaWorkingDirectory);

                _process = new Process
                {
                    EnableRaisingEvents = false,
                    StartInfo =
                    {
                        WorkingDirectory = vaWorkingDirectory,
                        FileName = "cmd.exe",
                        // Arguments = "bin\\VAServer.exe localhost:12340 " + vaPathConfig,
                        Arguments = "/c" + command,
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        RedirectStandardOutput = true,
                        // RedirectStandardInput = true,
                        RedirectStandardError = true
                    }
                };
                _process.OutputDataReceived += new DataReceivedEventHandler(DataReceived);
                _process.ErrorDataReceived += new DataReceivedEventHandler(ErrorReceived);

                _process.Start();

                _process.BeginOutputReadLine();
                _process.BeginErrorReadLine();
                Debug.Log("Successfully launched app in background.");
            }
            catch (Exception e)
            {
                Debug.LogError("Unable to launch app: " + e.Message);
            }
        }

        /// <summary>
        /// Convert Info Logs to Unity Log
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void DataReceived(object sender, DataReceivedEventArgs eventArgs)
        {
            if(eventArgs.Data.Contains("Error"))
                Debug.LogError($"<color=red><b>[VAError]</b> {eventArgs.Data}</color>");
            else
                Debug.Log($"<b>[VA]</b> {eventArgs.Data}");
        }

        /// <summary>
        /// Convert Error Logs to Unity Error Log
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void ErrorReceived(object sender, DataReceivedEventArgs eventArgs)
        {
            if (eventArgs.Data != null)
                Debug.LogError($"<color=red><b>[VAError]</b> {eventArgs.Data}</color>");
        }

   

        
    }
}