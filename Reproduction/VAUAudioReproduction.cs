﻿using UnityEngine;
using VA;

namespace VAUnity
{
	/// <summary>
	/// Base class for AudioReproductions - derived from VAUObject with string type IDs
	/// Provides basic Reproduction controls such as
	/// - muting
	/// - adjusting output gain
	/// of the reproduction.
	/// </summary>
	public class VAUAudioReproduction : VAUObject<string>
	{
		// TODO: Get reproduction modules from VAserver
		[Tooltip("Reproduction Module ID as specified in VA config file")]
		[SerializeField] private string moduleID;
		
		// Make these fields private so that derived classes have to use the public accessors
		[Tooltip("Control reproduction output gain")]
		[SerializeField] private double outputGain = 1.0;
		
		[Tooltip("Mute/unmute reproduction output")]
		[SerializeField] private bool isMuted = false;

		// Public access specifiers for outputGain ans isMuted
		public double OutputGain
		{
			set
			{
				outputGain = value;
				if (outputGain < 0) outputGain = 0;
				_va.SetReproductionModuleGain(ID, outputGain);
			}
			get
			{
				var outputGainFromVA = _va.GetReproductionModuleGain(ID);
				if (outputGainFromVA - outputGain > 1e-3)
				{
					Debug.LogWarning("Rendering Module gain of "+ID+" deviates from gain saved in component!");
					outputGain = outputGainFromVA;
				}

				return outputGain;
			}
		}
		public bool IsMuted
		{
			set
			{
				isMuted = value;
				_va.SetReproductionModuleMuted(ID, isMuted);
			}
			get
			{
				var mutedStateFromVA = _va.GetReproductionModuleMuted(ID);
				if (mutedStateFromVA != isMuted)
				{
					Debug.LogWarning("Rendering Module mute state of "+ID+" deviates from state saved in component!");
					isMuted = mutedStateFromVA;
				}

				return isMuted;
			}
		}

		/// <summary>
		/// Manually sets ReproductionModule ID.
		/// Currently (Oct-2021) IDs cannot be extracted from VAServer automatically
		/// </summary>
		protected override void Awake()
		{
			base.Awake();
			_id = moduleID;
		}

		/// <summary>
		/// Initialize reproduction as specified in editor
		/// </summary>
		private void Start ()
		{
			_va.SetReproductionModuleMuted( ID, isMuted );
			_va.SetReproductionModuleGain( ID, outputGain );
		}
	}
}
