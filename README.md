# VAUnity 

[VAUnity](https://git.rwth-aachen.de/ita/VAUnity) provides C# scripts to connect to the [Virtual Acoustics (VA)](https://www.virtualacoustics.org) real-time auralization framework. It is most commonly used to connect a VA auralization server with a Unity scene in order to create a synchronized audio-visual VR environment for an arbitrary combination of audio rendering and reproduction modules. The basic concept is to run a VA server and control it via the C# binding ([VACS](http://git.rwth-aachen.de/ita/VACS)) by adding classes to existing GameObjects in Unity. By enriching GameObjects with VA-specific classes, VA is triggered to create corresponding entities (like sources and receivers). Control information (such as translatory and rotatory movement) is propagated to the audio rendering instance(s). The Unity audio engine should be disabled. However, to enable VA support for Unity, the Unity project resources must be at hand and the scenes have to be extended manually by *VAUnity C# scripts* (mainly drag&drop). 


## Quick Help

- [Unity package](https://git.rwth-aachen.de/ita/vaunity_package) with examples. 
- Get an [overview](https://www.virtualacoustics.org/VA/overview/) on VA. 
- General [documentation](https://www.virtualacoustics.org/VA/documentation/) on VA and the available methods.
- Citation and previous [research](https://www.virtualacoustics.org/VA/research/) using VA. 

### License

Copyright 2015-2023 Institute of Technical Acoustics (ITA), RWTH Aachen University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use files of this project except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.



## Quick build guide for the unpatient and experienced users

The easiest way to use VAUnity is by using the [Unity package](https://git.rwth-aachen.de/ita/vaunity_package). 

- Create a new Unity project. 
- Clone the Unity package ***recursively*** to your computer. If you want to include VAUnity as a Unity package, clone it anywhere on your disk. Otherwise, it is recommended to clone it into the *Assets/Plugins/* folder in your Unity project. Make sure that the folder */Runtme/VAUnity* contains C# files. If not, double check if you cloned recursively, since VAUnity is included as a submodule.
- If you cloned the package to a random folder: go to the *Unity Editor->Package Manager->add->Add package from disk-> Navigate to folder -> select package.json file*. A detailed description is in the [Unity Documentation](https://docs.unity3d.com/2020.3/Documentation/Manual/upm-ui-local.html). A new folder called *Packages/VAUnity* should be visible in the project window. 
- If you cloned the package to the Assets folder, just open the Unity project and wait until everything is imported. 
- In the *Project* window, navigate to the VAUnity folder. Go to *VAUnity/Samples/BinauralFreeField* and open the *BinauralFreeField Scene*. 
- Import the TMP Essentials, if you don't have them in your project already. Think about upgrading your text to [TextMeshPro](https://docs.unity3d.com/Manual/com.unity.textmeshpro.html) in general.
- Put on your headphones and hit play. 
- The *VAServer* should start in the foreground. You can minimize the command window and enjoy the auralization. Use your mouse to rotate and WASD or the arrow keys to move. 
- Exit by hitting the Play button again. If you build the scene, you can also click on the *Quit* button to exit the game. 

Features included in the sample scene are a listener/sound receiver attached to the camera and a moving sound source. If you want to build your Unity project, use only relative paths. VAUnity will search for files located in the *Assets*, *Assets/Plugins* and *Packages* folder. 

# Unity & VA example scene tutorial

## Objective

This section describes how to create an example scene in Unity that controls a VA server for binaural audio rendering. In the end, the visual camera will act as a binaural sound receiver and can be moved from a first person viewpoint to inspect one static and one circulating sound source.


## Software installation

**Recommended**: Download or recursively clone the [VAUnity package](https://git.rwth-aachen.de/ita/vaunity_package) on your computer. If you want to include VAUnity as a Unity package, clone it anywhere on your disk. Otherwise, it is recommended to clone it into the *Assets/Plugins/* folder in your Unity project. Make sure that the folder */Runtme/VAUnity* contains C# files. If not, double check if you cloned recursively, since VAUnity is included as a submodule.

Otherwise, obtain a current VA full package from [virtualacoustics.org](http://www.virtualacoustics.org). Make sure that the subfolders `VACS_vxxxxx` and `VAUniy_vxxxxx` are included. Run a VA server with a binaural free-field rendering instance, e.g. by executing the `run_VAServer.bat` script (the untouched default VA configuration file will suffice).


## Unity project

Download, install and run Unity. Create a new project. 

### Using only VAUnity

Open the project folder in the file browser outside Unity. It should include a folder named `Assets`. Here, create a new directory `Plugins` and a subfolder `VA`. From your VA binary package, copy the directory `VACS_vxxxxx` into the newly created folder structure (`Assets/Plugins/VA`). Now, either clone the VAUnity repository into the `VA` directory or copy the files from the binary package (folder name: `VAUniy_vxxxxx`).
After Unity has detected the new assets the scripts are available and can be assigned to any GameObject as components. You can easily list the scripts as they all have the prefix `VAU`.

### Using the VAUnity Package (Recommended)

If you cloned the package to a random folder: go to the *Unity Editor->Package Manager->add->Add package from disk-> Navigate to folder -> select package.json file*. A detailed description is in the [Unity Documentation](https://docs.unity3d.com/2020.3/Documentation/Manual/upm-ui-local.html). A new folder called *Packages/VAUnity* should be visible in the project window. The package already contains a version of the VAServer, so you don't need to download it separately.


### Set up and connect to VA

To set up a connection to VA on startup, add the `VAUnity` script to a newly created empty GameObject named `VA`. Add a `VAAdapter` object to the VAUnity component. You can either use the one provided with the VAUnity Package at *VAUnity/Resources/VAAdapter/* or create a new one by *right clicking/create/VAUnity/VAAdapter*. 
You can include a `VAStarter` to define a specific configuration for the VAServer. Either use the `MyBinauralFreeField` object provided by the package in *VAUnity/Resources/VA_Configs* or create a new one by *right clicking/create/VAUnity/VAStarter*.

***If you DO NOT use the VAUnity package*** you need to create and use a VAStartet. Set the direct path to the VA working directory in your `VAStarter` object. The path should point at the VA full package you downloaded from the VA website. 


### Prepare resources

Create an empty GameObject called `HRTF` and add an existing C# script named `VAU Default HRIR` to the `HRTF` object.

Place a WAV audio sample in your assets folder. It will be used as signal source.
Create an empty GameObject called `AudioSignal` and add en existing C# script named `VAU Signal Source Audio File`. Type in the full name (including file ending) of the WAV file as FilePath. If you use a relative path, Unity will search for files located in the *Assets* and *Packages* folder. 

Create an empty GameObject called `Sound Source`. Add the `VAU Sound Source Generic` C# script. The Script will update the pose accordingly based on the transform, i.e. if the movement is connected to a tracking device (e.g. VR controller), the tracked data will be forwared to VA. Also, drag&trop the `AudioSignal` GameObject onto the `Signal Source` property of the `VAU Sound Source Generic` component. Check the *Play On Start* option on the `VAU Sound Source Generic`.


### Listener

Drag&drop the `VAU Sound Receiver` script onto the camera rig or another GameObject to create a dynamic listener in VA. The Script will update the pose accordingly based on the transform, i.e. if the movement is connected to a tracking device (e.g. VR glasses), the tracked data will be forwared to VA. To enable binaural rendering, drag&drop the `HRTF` GameObject onto the `Directivity` parameter of the `VAU Sound Receiver` component. 



### Auralization modes

You can control the auralization modes via the `VAU Auralizaton Mode Global` script. Auralization modes are either controlling sound sources, listeners or are set globally. VA will only auralize a certain effect, if the corresponding mode is activated for all three components, i.e. a logic AND combination is applied.

