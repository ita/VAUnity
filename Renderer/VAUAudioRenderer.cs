﻿using UnityEngine;

namespace VAUnity
{
	/// <summary>
	/// Base class for AudioRenderers - derived from VAUObject with string type IDs
	/// Provides basic Renderer controls such as
	/// - muting
	/// - adjusting output gain
	/// of the renderer.
	/// </summary>
	public abstract class VAUAudioRenderer : VAUObject<string>
	{
		// TODO: Get rendering modules from VAserver
		[Tooltip("Rendering Module ID as specified in VA config file")]
		[SerializeField] private string moduleID;
		
		// Make these fields private so that derived classes have to use the public accessors
		[Tooltip("Control rendering output gain")]
		[SerializeField] private double outputGain = 1.0;
		
		[Tooltip("Mute/unmute rendering output")]
		[SerializeField] private bool isMuted = false;

		public abstract bool IsSoundSourceExplicitRenderer { get; }
		public virtual int CreateSoundSourceExplicitRenderer()
		{
			return -1;
		}
		
		// Public access specifiers for outputGain ans isMuted
		public double OutputGain
		{
			set
			{
				outputGain = value;
				if (outputGain < 0) outputGain = 0;
				_va.SetRenderingModuleGain(ID, outputGain);
			}
			get
			{
				var outputGainFromVA = _va.GetRenderingModuleGain(ID);
				if (outputGainFromVA - outputGain > 1e-3)
				{
					Debug.LogWarning("Rendering Module gain of "+ID+" deviates from gain saved in component!");
					outputGain = outputGainFromVA;
				}

				return outputGain;
			}
		}
		public bool IsMuted
		{
			set
			{
				isMuted = value;
				_va.SetRenderingModuleMuted(ID, isMuted);
			}
			get
			{
				var mutedStateFromVA = _va.GetRenderingModuleMuted(ID);
				if (mutedStateFromVA != isMuted)
				{
					Debug.LogWarning("Rendering Module mute state of "+ID+" deviates from state saved in component!");
					isMuted = mutedStateFromVA;
				}

				return isMuted;
			}
		}

		/// <summary>
		/// Manually sets RendererModule ID.
		/// Currently (Oct-2021) IDs cannot be extracted from VAServer automatically
		/// </summary>
		protected override void Awake()
		{
			base.Awake();
			_id = moduleID;
		}

		/// <summary>
		/// Initialize renderer as specified in editor
		/// </summary>
		protected virtual void Start ()
		{
			_va.SetRenderingModuleMuted( ID, isMuted );
			_va.SetRenderingModuleGain( ID, outputGain );
		}
	}
}
