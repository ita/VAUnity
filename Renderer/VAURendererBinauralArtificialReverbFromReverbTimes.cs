﻿using UnityEngine;

namespace VAUnity
{
	/// <summary>
	/// Supposed to implement a more direct version of reverberation time updates, which is not based on AudioReverbZones
	/// </summary>
	public class VAURendererBinauralArtificialReverbFromReverbTimes : VAURendererBinauralArtificialReverb
	{
		[Tooltip("Room volume in m^3. Minimum 600.0 m^3")]
		[SerializeField] private double roomVolume = MINRoomVolume;

		[Tooltip("Room surface area in m^2. Minimum 220.0 m^2")]
		[SerializeField] private double roomSurfaceArea = MINSurfaceArea;

		// TODO: Turn into List and implement reverb time updates based on [low, mid, high] and octave band representation
		[Tooltip("Reverberation time in seconds. Minimum 0.3 s")]
		[SerializeField] private double roomReverberationTime = MINReverbTime;

		// Public access specifiers
		public double RoomVolume
		{
			get => roomVolume;
			set
			{
				roomVolume = value > MINRoomVolume ? value : MINRoomVolume;
				_va.SetArtificialRoomVolume(ID, roomVolume);
			}
		}
		public double RoomSurfaceArea
		{
			get => roomSurfaceArea;
			set
			{
				roomSurfaceArea = value > MINSurfaceArea ? value : MINSurfaceArea;
				_va.SetArtificialSurfaceArea(ID, roomSurfaceArea);
			}
		}
		public double RoomReverberationTime
		{
			get => roomReverberationTime;
			set
			{
				roomReverberationTime = value > MINReverbTime ? value : MINReverbTime;
				_va.SetArtificialReverberationTime(ID, roomReverberationTime);
			}
		}

		/// <summary>
		/// Set initial configuration of the BinauralArtificialReverb renderer
		/// </summary>
		protected override void Awake()
		{
			base.Awake();

			if (roomVolume >= MINRoomVolume)
				_va.SetArtificialRoomVolume(ID, roomVolume);
			if (roomSurfaceArea >= MINSurfaceArea)
				_va.SetArtificialSurfaceArea(ID, roomSurfaceArea);
			if (roomReverberationTime > MINReverbTime)
				_va.SetArtificialReverberationTime(ID, roomReverberationTime);
		}
	}
}
