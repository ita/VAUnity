﻿using UnityEngine;

namespace VAUnity
{
    public class VAURendererGenericPathFromFile : VAURendererGenericPath
    {
		
        [Tooltip("Generic path renderer default impulse response file")]
        [SerializeField] private string irFilePath = "";
		
        protected void Update()
        {
            if (!soundSource || !soundReceiver || UpdatePerformed) return;
			
            _va.UpdateGenericPathFromFile (ID, soundSource.ID, soundReceiver.ID, irFilePath);
            if( soundSource.ID > 0 && soundReceiver.ID > 0 )
                UpdatePerformed = true;
        }
    }
}