﻿using UnityEngine;

namespace VAUnity
{
    /// <summary>
    /// Common class for BinauralArtificialReverb renderer configurations.
    /// Defines constants for minimum acceptable config parameters.
    /// </summary>
    public class VAURendererBinauralArtificialReverb : VAUAudioRenderer
    {
        protected const double MINRoomVolume = 600.0;
        protected const double MINSurfaceArea = 220.0;
        protected const double MINReverbTime = 0.3;
        public override bool IsSoundSourceExplicitRenderer => false;
    }
}