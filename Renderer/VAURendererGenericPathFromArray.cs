﻿using UnityEngine;

namespace VAUnity
{
    public class VAURendererGenericPathFromArray : VAURendererGenericPath
    {
        [Tooltip("Generic path renderer default impulse response file")] [SerializeField]
        private float[] irLeft = {0, 0, 0, 0, 0, 0, 0, 0, 0, 1};
		
        [Tooltip("Generic path renderer default impulse response file")]
        [SerializeField] private float[] irRight = {1, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		
        [Tooltip("Add additional delay to convolution process.")]
        [SerializeField] private float delay = 0;
		
        protected void Update()
        {
            if (!soundSource || !soundReceiver || UpdatePerformed) return;

            _va.UpdateGenericPath(ID, soundSource.ID, soundReceiver.ID, 0, delay, irLeft.Length, irLeft);
            _va.UpdateGenericPath(ID, soundSource.ID, soundReceiver.ID, 0, delay, irRight.Length, irRight);
            UpdatePerformed = true;
        }
    }
}