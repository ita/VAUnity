﻿using UnityEngine;

namespace VAUnity
{
    public class VAURendererAmbientMixer : VAUAudioRenderer
    {
        private bool _isInit = false;
        public override bool IsSoundSourceExplicitRenderer => true;
        public int explicitSoundSourceId;
        public override int CreateSoundSourceExplicitRenderer()
        {
            if (!_isInit)
                explicitSoundSourceId = _va.CreateSoundSourceExplicitRenderer(ID, "HOAInput");
            _isInit = true;
            return explicitSoundSourceId;
        }
    }
}