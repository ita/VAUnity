﻿using UnityEngine;

namespace VAUnity
{
	public class VAURendererGenericPath : VAUAudioRenderer
	{
		[Tooltip("Generic path renderer channel number. Not implemented yet.")]
		[SerializeField] protected int numberOfChannels = 2;

		[Tooltip("Generic path renderer channel number. Not implemented yet.")]
		[SerializeField] protected int filterLengthSamples = 88200;

		[Tooltip("Generic path renderer sampling rate. Not implemented yet.")]
		[SerializeField] protected double samplingRate = 44100;


		[Tooltip("Generic path sound source")]
		[SerializeField] protected VAUSoundSource soundSource;

		[Tooltip("Generic path sound receiver")]
		[SerializeField] protected VAUSoundReceiver soundReceiver;

		protected bool UpdatePerformed = true;
		public override bool IsSoundSourceExplicitRenderer => false;
	}
}
