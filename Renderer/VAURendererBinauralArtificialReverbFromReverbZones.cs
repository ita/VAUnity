﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace VAUnity
{
    public class VAURendererBinauralArtificialReverbFromReverbZones : VAURendererBinauralArtificialReverb
    { 
        [Tooltip("Max. number of reverb zones considered for calculation")]
        [SerializeField] private int nMaxReverbZones = 2;
        public int NMaxReverbZones
        {
            get => nMaxReverbZones;
            set
            {
                if (value < 0)
                    Debug.LogWarning("Max number of reverb zones must be >=0. Setting to 0!");
                nMaxReverbZones = value < 0 ? 0 : value;
            }
        }

        [SerializeField] private VAUSoundReceiver receiver;
        private Transform _receiverTransform;

        [Tooltip("Add AudioReverbZones here. Will be filled automatically if left empty.")]
        [SerializeField] private List<AudioReverbZone> audioReverbZones;

        [Tooltip("Low frequency range factor for reverberation time. Compare to decayHFRatio of AudioReverbZone.")]
        [SerializeField] private List<float> decayLFRatio;

        private float[] _reverbTime = new float[] { 0, 0, 0 };

        protected override void Start()
        {
            base.Start();
            
            // Only need the transform of the sound receiver
            _receiverTransform = receiver.transform;
            
            if (!(audioReverbZones.Count > 0))
                audioReverbZones = (FindObjectsOfType(typeof(AudioReverbZone)) as AudioReverbZone[])?.ToList();

            if (decayLFRatio.Count == 1)
            {
                for (int i = 0; i < audioReverbZones.Count - 1; i++)
                {
                    decayLFRatio.Add(decayLFRatio[0]);
                }
            }

            if (audioReverbZones.Count != decayLFRatio.Count)
                throw new UnityException("Number of decayLFRatios does not match number of AudioReverbZones. " +
                                         "Enter a decay ratio for each reverb zone, or only that will be used for all zones.");
            
            CalcReverbTimeFromActiveReverbZones();
        }

        private void Update()
        {
            if (!IsMuted && _receiverTransform.hasChanged)
                CalcReverbTimeFromActiveReverbZones();
        }

        /// <summary>
        /// Iterates through registered reverb zones and averages the reverb times of all applicable reverb zones.
        /// Active reverb zones are determined by effect distance and linearly interpolated between min and max distance.
        /// Intended for [low, mid, high] frequency split of reverb times, but currently (Oct 2021) employs a weighted average
        /// defined by AudioReverbZone.decayHFRatio and the renderers decayLFRatio for high and low, respectively. 
        /// </summary>
        private void CalcReverbTimeFromActiveReverbZones()
        {
            var newReverbTimes = new float[3] {0, 0, 0};
            float receiverDistance, maxDist, minDist;
            var i = 0;
            foreach (var reverbZone in audioReverbZones)
            {
                if (i > nMaxReverbZones) break;
                receiverDistance = Vector3.Distance(reverbZone.transform.position, _receiverTransform.position);
                maxDist = reverbZone.maxDistance;
                minDist = reverbZone.minDistance;

                if (receiverDistance > maxDist)  // Nothing to do if Listener is not inside ReverbZone
                    continue;
                
                // linearly interpolate reverb time decay between min and max distance
                newReverbTimes[1] += reverbZone.decayTime * (receiverDistance < reverbZone.minDistance ? 1
                                                    : (maxDist - receiverDistance) / (maxDist - minDist)); // mid freq
                
                newReverbTimes[0] += decayLFRatio[i] * newReverbTimes[1]; // low freq
                newReverbTimes[2] += reverbZone.decayHFRatio * newReverbTimes[1];  // high freq
                
                i++;
            }

            // Average across all reverb zones (i contains number of ReverbZones)
            for (int j = 0; j < newReverbTimes.Length; j++)
                newReverbTimes[j] /= i;

            // update reverb times
            bool updateReverbTimes = false;
            for (i = 0; i < _reverbTime.Length; i++)
            {
                if (newReverbTimes[i] - _reverbTime[i] < 1e-3) continue;
                _reverbTime[i] = newReverbTimes[i];
                updateReverbTimes = true;
            }

            if (!updateReverbTimes) return;
            
            // TODO: Update VACS to use array version (Single value is deprecated)
            float reverbTimeUpdate = (_reverbTime[0] + _reverbTime[1] + _reverbTime[2]) / 3;

            if (reverbTimeUpdate < MINReverbTime)
            {
                Debug.LogWarning("Calculated reverberation time of " + reverbTimeUpdate + " is smaller than " +
                                 "minimum allowed (" + MINReverbTime + "). Resetting to minimum value!");
                reverbTimeUpdate = (float) MINReverbTime;
            };
            _va.SetArtificialReverberationTime(ID, reverbTimeUpdate);
        }
    }
}