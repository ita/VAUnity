﻿namespace VAUnity
{
    public class VAUAuralizationModeGlobal : VAUAuralizationMode
    {
        protected override void Awake()
        {
            base.Awake();
            AuraStringChanged += OnGlobalAuralizationModeChanged;
        }
        
        private void OnDisable()
        {
            AuraStringChanged -= OnGlobalAuralizationModeChanged;
        }
        
        private void OnGlobalAuralizationModeChanged(string AuraMode)
        {
            _va.SetGlobalAuralizationMode(AuraMode);
        }
    }
}