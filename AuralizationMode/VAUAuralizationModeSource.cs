﻿using UnityEngine;
using VA;

namespace VAUnity
{
    [RequireComponent(typeof(VAUSoundSource))]
    public class VAUAuralizationModeSource : VAUAuralizationMode
    {
        private VAUSoundSource[] _vauSoundSources;

        protected override void Awake()
        {
            base.Awake();
            _vauSoundSources = FindObjectsOfType<VAUSoundSource>();
            AuraStringChanged += OnSoundSourceAuralizationModeChanged;
        }
        private void OnDisable()
        {
            AuraStringChanged -= OnSoundSourceAuralizationModeChanged;
        }
        
        private void OnSoundSourceAuralizationModeChanged(string AuraMode)
        {
            
            foreach (var vauSoundSource in _vauSoundSources)
            {
                _va.SetSoundSourceAuralizationMode(vauSoundSource.ID, AuraMode);
            }
        }


    }
}
