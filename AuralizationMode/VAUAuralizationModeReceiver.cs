﻿using UnityEngine;
using VA;

namespace VAUnity
{
    [RequireComponent(typeof(VAUSoundReceiver))]
    public class VAUAuralizationModeReceiver : VAUAuralizationMode
    {
        private VAUSoundReceiver[] _vauSoundReceivers;

        //private VANet _va = null; 
        
        protected override void Awake()
        {
            base.Awake();
            _vauSoundReceivers = GetComponents<VAUSoundReceiver>();
            AuraStringChanged += OnSoundReceiverAuralizationModeChanged;
        }
        
        private void OnDisable()
        {
            AuraStringChanged -= OnSoundReceiverAuralizationModeChanged;
        }
        
        private void OnSoundReceiverAuralizationModeChanged(string AuraMode)
        {
            foreach (var vauSoundReceiver in _vauSoundReceivers)
            {
                _va.SetSoundReceiverAuralizationMode(vauSoundReceiver.ID, AuraMode);
            }
        }
    }
}
