﻿using UnityEngine;
using VA;

namespace VAUnity
{
    /// <summary>
    /// Abstract class to derive most VAUnity MonoBehaviours from.
    /// Provides protected access to VANet Singleton.
    /// Holds VAServer generated Object/Module IDs.
    /// </summary>
    /// <typeparam name="T">Type of the object's ID in VA</typeparam>
    public abstract class VAUObject<T> : MonoBehaviour
    {
        protected T _id = default(T);
        public T ID => _id;
        protected VANet _va;

        protected virtual void Awake()
        {
            _va = VAUnity.VA;
        }
    }
}