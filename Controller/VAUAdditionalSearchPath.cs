﻿using UnityEngine;

namespace VAUnity
{
    public class VAUAdditionalSearchPath : MonoBehaviour
    {
        public string path = "";

        private void Start ()
        {
            VAUnity.VA.AddSearchPath( path );
        }
    }
}
